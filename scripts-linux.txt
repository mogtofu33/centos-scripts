# Lien symbolique
ln -s [TARGET DIRECTORY OR FILE] ./[SHORTCUT]

#remplacer script dans fichier
sed 's#<script [^>]*></script>##' index.php

# remplacer dans tous les fichiers
find . -name "index.php" -type f|xargs sed -i -e 's#<script [^>]*></script>##'
find /home/arthurp/domains -name "index.php" -type f|xargs sed -i -e 's#<script [^>]*></script>##'

# trouver
grep -r --exclude=*.{css,js} "cover" .
grep -Ir --exclude="*\.svn*" "pattern" *

# trouver fichier dans dossier 777
find /home/ -type d -perm 777 -exec find {} -name "*.php" \;

# trouver texte dans fichier : affiche ligne et fichier
find /var/www/vhosts -name 'settings.php' |xargs grep -l "cacherouter" 

find /chemin/rep -name "*" -exec grep -Hnr "string" {} \;

	recherche la cha�ne (string) en affichant le chemin (-H) et le num�ro de ligne (-n) si la cha�ne est trouv�e. 
	n -> dans le fichier
	r -> recherche r�cursive (ie dans les sous r�pertoires)
	i -> insensible � la casse 

find /home -type f -name php.ini -exec grep -Hnr "session.save_handler" {} \;

# remplace une chaine par une autre
find /home/arthurp/domains -name "uc-packing-slip.tpl.php" -type f|xargs sed -i -e 's/profile_load_profile(&$order/profile_load_profile($order/g'
find -name \*.tex -type f|xargs sed -i -e '...'

# virtualmin, mass edit php.ini
find /home -name php.ini | xargs sed -i "s/memory_limit =.*/memory_limit = 32M/"
# idem error
find /home -name php.ini | xargs sed -i "s/display_errors =.*/display_errors = Off/"
find /home -name php.ini | xargs sed -i "s/max_input_time =.*/max_input_time = 60/"

# voir taille r�pertoires
du -h

# tailles dossier actuels de tous les elements avec leur descendants
du -hsc *
# -c = + total

# avec tri par taille
du * | sort -n
du -hs * | sort -n

# avec ImageMagick, convertir des jpg, enleve infos + qualite + progressif
mogrify -verbose -resize 1200 -strip -quality 75 -interlace plane *.jpg
# png
mogrify -verbose -colors 256 -depth 8 -quality 70 *.png

# recursive
find [directory] -type f -name '*.gif' | xargs mogrify ...

# replace file by empty one
find [directory] -type f -name '*.pdf' | while read FILE; do echo > "$FILE"; done

# trouver des fichiers (image + de 2M)
find -name "*.jpg" -size +2000k

# cumul, retaille tous les jpg de + de 400K
find [directory] -type f -name '*.jpg'  -size +1000k | xargs mogrify -verbose -resize 1200 -strip -quality 75 -interlace plane *.jpg

# multiples cron
for ((n=0;n<30;n++)); do drush -v core-cron; done

# sql import multiples
find . -name '*.sql' | awk '{ print "source",$0 }' | mysql --batch -u {username} -p {dbname}
ls -1 *.sql | awk '{ print "source",$0 }' | mysql --batch -u {username} -p{password} {dbname}
for SQL in *.sql; do DB=${SQL/\.sql/}; echo importing $DB; mysql $DB < $SQL; done

mysql -u root -ppassword DATABASE_NAME < database1-2011-01-15.sql

find . -name '*.sql' | awk '{ print "source",$0 }' | mysql --batch -u pm_rec_evolphp -pKoxphiCufoc0 pm_recette_evolphp

# diff r�pertoires r�cursifs, seulement et diff, pas cache, noms
diff -rqb /rep1/ /rep2/ | grep -E "Seulement|diff" | grep -Ev "cache|git" > diff.txt

#To recursively give directories read&execute privileges:
find /path/to/base/dir -type d -exec chmod 755 {} +
#To recursively give files read privileges:
find /path/to/base/dir -type f -exec chmod 644 {} +