#!/bin/bash

PROGNAME=${0##*/} 
PROGVERSION=0.1

# Environment values.
MYSQL=`which mysql`

OLDCONF=$(dpkg -l|grep "^rc"|awk '{print $2}')
CURKERNEL=$(uname -r|sed 's/-*[a-z]//g'|sed 's/-386//g')
LINUXPKG="linux-(image|headers|ubuntu-modules|restricted-modules)"
METALINUXPKG="linux-(image|headers|restricted-modules)-(generic|i386|server|common|rt|xen)"
OLDKERNELS=$(dpkg -l|awk '{print $2}'|grep -E $LINUXPKG |grep -vE $METALINUXPKG|grep -v $CURKERNEL)
YELLOW="\033[1;33m"
RED="\033[0;31m"
ENDCOLOR="\033[0m"

print_help() {
cat <<-HELP
Clean script
 Options with arguments:
  --purge             Removing old config & Kernels files
  --reset             SSH history & other cruft
  --vhost             Removing all websites and db

 Options:
  -h,  --help         Display this help and exit
  -V,  --version      Output version information and exit


Usage: (sudo) bash ${0##*/}

Example: (sudo) bash ${0##*/} --purge --vhost

HELP
exit 0
}

# Default
purge=0
reset=0
vhost=0

# Check command flags invoked and store possible arguments.
for i in "$@"
do
case $i in
    -h|--help) print_help >&2; safe_exit ;;
    -V|--version) out "$PROGNAME v$PROGVERSION"; safe_exit ;;
    --purge) purge=1 ;;
    --reset) reset=1 ;;
    --vhost) vhost=1 ;;
esac
done

echo -e $YELLOW"Cleaning apt cache..."$ENDCOLOR
sudo aptitude clean

if [[ $purge == 1 ]]; then
  echo -e $YELLOW"Removing old config files..."$ENDCOLOR
  sudo aptitude purge $OLDCONF

  echo -e $YELLOW"Removing old kernels..."$ENDCOLOR
  sudo aptitude purge $OLDKERNELS
fi


# Stop logging services
sudo service rsyslog stop

# Flush mail queue
#/usr/sbin/postsuper -r ALL

# Remove Virtualbox specific files
#/bin/rm -rf /usr/src/vboxguest*

# Remove under tmp directory
echo -e $YELLOW"Removing tmp files..."$ENDCOLOR
sudo /bin/rm -rf /tmp/*
sudo /bin/rm -rf /var/tmp/*

# Remove other cache or tmp files
sudo /bin/rm -rf /root/.drush/cache/*
sudo /bin/rm -rf /home/mog/.drush/cache
sudo /bin/rm -rf /home/mog/.composer/cache
sudo /bin/rm -rf /var/lib/phpmyadmin/tmp/*
sudo /bin/rm -rf /var/lib/phpmyadmin/upload/*

# Remove history
echo -e $YELLOW"Clean history..."$ENDCOLOR
history -cw
> ~/.bash_history

# Cleanup log files
echo -e $YELLOW"Clean logs..."$ENDCOLOR
sudo /usr/sbin/logrotate -f /etc/logrotate.conf

sudo /bin/rm -f /var/log/*.gz
sudo /bin/rm -f /var/log/apache2/*.gz

#### Reset net for cloned VM
if [[ $reset == 1 ]]; then
  echo -e $YELLOW"Remove the SSH host keys..."$ENDCOLOR
  sudo /bin/rm –f /etc/ssh/*key*

  echo -e $YELLOW"Remove the root user’s SSH history & other cruft..."$ENDCOLOR
  sudo /bin/rm -rf /root/.ssh/
  sudo /bin/rm -rf ~.ssh/
fi

if [[ $vhost == 1 ]]; then
  echo -e $YELLOW"Remove vhost config, websites and databases..."$ENDCOLOR
  sudo /usr/bin/find /var/www/html/ -mindepth 1 -type d -delete
  
  DBS="$($MYSQL -Bse 'SHOW databases;')"
  CORE_DB=(information_schema mysql performance_schema phpmyadmin)
  for db in ${DBS[@]}
  do
    match=0
    for core_db in "${CORE_DB[@]}"; do
        if [[ $core_db == "$db" ]]; then
           match=1
           break
        fi
    done
    if [[ $match = 0 ]]; then
        echo -e $YELLOW"Drop db $db"$ENDCOLOR
        echo "DROP DATABASE $db;" | $MYSQL
    fi
  done

### End reset

# Start logging services
sudo /sbin/service rsyslog start

echo -e $YELLOW"Script Finished!"$ENDCOLOR
exit;