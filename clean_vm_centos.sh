#!/bin/bash

PROGNAME=${0##*/} 
PROGVERSION=0.1

# Environment values.
MYSQL=`which mysql`

YELLOW="\033[1;33m"
RED="\033[0;31m"
ENDCOLOR="\033[0m"

print_help() {
cat <<-HELP
Clean script
 Options with arguments:
  --purge             Removing old config & Kernels files
  --reset             SSH history & other cruft
  --vhost             Removing all websites and db

 Options:
  -h,  --help         Display this help and exit
  -V,  --version      Output version information and exit


Usage: (sudo) bash ${0##*/}

Example: (sudo) bash ${0##*/} --purge --vhost

HELP
exit 0
}

# A non-destructive exit for when the script exits naturally.
safe_exit() {
  trap - INT TERM EXIT
  exit
}

# Helpers
out() {
  ((quiet)) && return

  local message="$@"
  if ((piped)); then
    message=$(echo $message | sed '
      s/\\[0-9]\{3\}\[[0-9]\(;[0-9]\{2\}\)\?m//g;
      s/✖/Error:/g;
      s/✔/Success:/g;
    ')
  fi
  printf '%b\n' "$message";
}
die() { out "$@"; exit 1; } >&2
err() { out " \033[1;31m✖\033[0m  $@"; } >&2
success() { out " \033[1;32m✔\033[0m  $@"; }

# Default
purge=1
reset=0
vhost=0

# Check command flags invoked and store possible arguments.
for i in "$@"
do
case $i in
    -h|--help) print_help >&2; safe_exit ;;
    -V|--version) out "$PROGNAME v$PROGVERSION"; safe_exit ;;
    --purge) purge=1 ;;
    --reset) reset=1 ;;
    --vhost) vhost=1 ;;
esac
done

# Stop logging services
echo -e $YELLOW"Stop logging services..."$ENDCOLOR
/usr/sbin/service rsyslog stop
/usr/sbin/service auditd stop

# Purge
if [[ $purge == 1 ]]; then
  echo -e $YELLOW"Removing old kernels..."$ENDCOLOR
  /bin/package-cleanup -y --oldkernels --count=1
  
  echo -e $YELLOW"Removing old config files and clean Yum..."$ENDCOLOR
  /usr/bin/yum -y remove gcc kernel-devel kernel-headers

fi
echo -e $YELLOW"Cleaning yum cache..."$ENDCOLOR
/usr/bin/yum -y clean all

# Stop servers
echo -e $YELLOW"Stop servers..."$ENDCOLOR
/usr/bin/systemctl stop mariadb
/usr/bin/systemctl stop memcached
/usr/bin/systemctl stop httpd
/usr/bin/systemctl stop mailcatcher

# Flush mail queue
echo -e $YELLOW"Flush mail queue..."$ENDCOLOR
/usr/sbin/postsuper -r ALL

# Remove Virtualbox specific files
#echo -e $YELLOW"Remove Virtualbox specific files..."$ENDCOLOR
#/bin/rm -rf /usr/src/vboxguest*

# Remove tmp and cache
echo -e $YELLOW"Removing tmp files..."$ENDCOLOR
/bin/rm -rf /tmp/*
/bin/rm -rf /var/tmp/*

# Remove other cache or tmp files
/bin/rm -rf /var/lib/phpMyAdmin/upload/*
/bin/rm -rf /var/www/xhprof/output/*
/bin/rm -rf /var/www/xdebug/*

# Third party tools
/usr/local/bin/composer clear-cache
/bin/rm -rf /root/.drush/cache/

# Clean history
echo -e $YELLOW"Clean history..."$ENDCOLOR
history -cw
> ~/.bash_history
> ~/.mysql_history
> ~/.boris_history

# Cleanup log files
echo -e $YELLOW"Clean logs..."$ENDCOLOR
/bin/rm -f /var/log/dmesg.old
/usr/sbin/logrotate -f /etc/logrotate.conf

# Reset net for cloned VM
if [[ $reset == 1 ]]; then
  echo -e $YELLOW"Remove the SSH host keys..."$ENDCOLOR
  /bin/rm –rf /etc/ssh/*key*

  echo -e $YELLOW"Remove the root user’s SSH history & other cruft..."$ENDCOLOR
  /bin/rm –rf /root/.ssh/
  /bin/rm -rf /root/anaconda-ks.cfg

  # Remove eth config
  echo -e $YELLOW"Remove eth config..."$ENDCOLOR
  /bin/rm -f /etc/udev/rules.d/70*
  /bin/sed -i '/^UUID/d'   /etc/sysconfig/network-scripts/ifcfg-enp0s3
  /bin/sed -i '/^HWADDR/d' /etc/sysconfig/network-scripts/ifcfg-enp0s3
  /bin/sed -i '/^IPADDR/d' /etc/sysconfig/network-scripts/ifcfg-enp0s3

# End reset
fi

# Remove vhost config, websites and databases
if [[ $vhost == 1 ]]; then
  echo -e $YELLOW"Remove vhost config, websites and databases..."$ENDCOLOR
  #/usr/bin/find /var/www/html/ -mindepth 1 -type d -delete
  
  DBS="$($MYSQL -u root -proot -Bse 'SHOW databases;')"
  CORE_DB=(information_schema mysql performance_schema phpmyadmin)
  for db in ${DBS[@]}
  do
    match=0
    for core_db in "${CORE_DB[@]}"; do
        if [[ $core_db == "$db" ]]; then
           match=1
           break
        fi
    done
    if [[ $match = 0 ]]; then
        echo -e $YELLOW"Drop db $db"$ENDCOLOR
        #echo "DROP DATABASE $db;" | $MYSQL
    fi
  done
fi

# Start logging services
echo -e $YELLOW"Start logging services..."$ENDCOLOR
/usr/sbin/service rsyslog start
/usr/sbin/service auditd start

# Start servers services
echo -e $YELLOW"Start servers services..."$ENDCOLOR
/usr/bin/systemctl start mailcatcher
/usr/bin/systemctl start memcached
/usr/bin/systemctl start mariadb
/usr/bin/systemctl start httpd

echo -e $YELLOW"Clean Finished!"$ENDCOLOR
exit;