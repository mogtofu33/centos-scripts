#!/bin/sh
#Backup mysql sur 5 jours
Mysql_User="USER"
Mysql_Paswd="PASS"
Mysql_host="localhost"
# Emplacemment des different prog utilis�, laissez tel quel si vous n'avez rien bidouill� (sinon updatedb & locate)
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
MYSQLCHECK="$(which mysqlcheck)"
CHOWN="$(which chown)"
CHMOD="$(which chmod)"
GZIP="$(which gzip)"

# Emplacemment du dossier de backup local
DEST="/home/backup"

#Repertoire ou on met le .sql
DEST_mysql="$DEST/mysql"

#Date du jour
NOW="$(date +"%d-%m-%Y")"

# Databases a ne pas sauvegarder s�parer par des espaces
IGGY=""

# On initialise les variables
FILE=""
DBS=""

# on optimize les tables 
$MYSQLCHECK -u $Mysql_User -h $Mysql_host -p$Mysql_Paswd --auto-repair --check --optimize --all-databases

#on cr�e le repertoire
[ ! -d "$DEST_mysql/$NOW" ] && mkdir -p "$DEST_mysql/$NOW" || :

#On limite l'acc�s au root uniquemment
#$CHOWN 0.0 -R $DEST_mysql
#$CHMOD 0600 $DEST_mysql

# On liste les bases de donnees
DBS="$($MYSQL -u $Mysql_User -h $Mysql_host -p$Mysql_Paswd -Bse 'show databases')"

for db in $DBS
do
    skipdb=-1
    if [ "$IGGY" != "" ];
    then
        for i in $IGGY
        do
            [ "$db" == "$i" ] && skipdb=1 || :
        done
    fi

    if [ "$skipdb" == "-1" ] ; then
        FILE="$DEST_mysql/$NOW/$db.gz"
        # On boucle, et on dump toutes les bases et on les compresse
        $MYSQLDUMP -u $Mysql_User -h $Mysql_host -p$Mysql_Paswd $db | $GZIP -9 > $FILE
    fi
done

#BACKUP des bases mysql par FTP
#nombre de jour de backup � conserver
j=5
j_a_delete=$(date +%d-%m-%Y --date "$j days ago")

## Conf des rep de backup pour le ftp
DIR_BACKUP_mysql=$DEST_mysql/$NOW
DIR_DIST_BACKUP_mysql='/backup/mysql/'

DIR_BACKUP_sites=$DEST/$NOW
DIR_DIST_BACKUP_sites='/backup/'

#Configuration de pass ftp
loginftp="LOGIN"
motdepassftp="PASS"
host_ftp="HOST"

## Upload sur le ftp
yafc $loginftp:$motdepassftp@$host_ftp <<**
cd $DIR_DIST_BACKUP_mysql
lcd $DIR_BACKUP_mysql
mkdir $NOW
cd $NOW
put -r *
cd ..
rm -r $j_a_delete
cd $DIR_DIST_BACKUP_sites
lcd $DIR_BACKUP_sites
mkdir $NOW
cd $NOW
put -r *
cd ..
rm -r $j_a_delete
quit
**

#On delete tout
#cd $DEST_mysql
#rm -r *

rm -r "$DEST_mysql/$j_a_delete"