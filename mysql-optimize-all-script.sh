#!/bin/sh
# mysql optimize on all table
# Mog 23/11/2010
# edited for MySQL 5.5 on 08/2014

mysqlcheck -u USER -pPASS --auto-repair --check --all-databases && mysqlcheck -u USER -pPASS --auto-repair --optimize --all-databases
