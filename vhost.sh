#!/bin/bash

PROGNAME=${0##*/} 
PROGVERSION=0.1

YELLOW="\033[1;33m"
RED="\033[0;31m"
GREEN="\033[0;32m"
ENDCOLOR="\033[0m"

MYSQL=`which mysql`
A2EN=`which a2ensite`
A2DIS=`which a2dissite`

APACHE_SITE_DIR="/etc/httpd/sites-available"
APACHE_SITE_EN_DIR="/etc/httpd/sites-enabled/"
SERVER_ADMIN="webmaster@localhost"
APACHE_USER="apache:apache"
PORT=80
VHOST=""
DOCUMENT_ROOT="/var/www/html/"
DELETE=0
DB_HOST="localhost"
DB_USER="root"
DB_PASS="root"
SUFFIX="dvm"

# Functions
############

print_help() {
cat <<-HELP
Apache vHost creation on CentOS 7+
 Options with arguments:
  -c            Create vhost by name
  -s            Server uri suffix, default dvm (test.dvm)
  -d            Delete vhost by name
  -a            Owner email, default webmaster@localhost
  -p            Port, default 80
  -r            Document root, default /var/www/html/
  -w            Apache user and group, default apache:apache

 Options:
  -h,  --help         Display this help and exit


Usage: (sudo) bash ${0##*/}

Example: (sudo) bash ${0##*/} -c drupal8

HELP
exit 0
}

# Main
#######

while getopts "a:p:c:d:r:h:w" opt; do
  case $opt in
    a)
      # Set server admin
      SERVER_ADMIN=$OPTARG
      ;;
    p)
      # Set port
      PORT=$OPTARG
      ;;
    c)
      # Set vhost name
      VHOST=$OPTARG
      ;;
    d)
      # Set vhost name
      VHOST=$OPTARG
      DELETE=1
      ;;
    r)
      # Set document root
      DOCUMENT_ROOT=$OPTARG
      ;;
    w)
      # Set apache user
      APACHE_USER=$OPTARG
      ;;
    s)
      # Set document root
      SUFFIX=$OPTARG
      ;;
    h)
      print_help
      exit
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

# Errors
if [ -z "$VHOST" ]; then
  echo -e $RED"You must specify a vhost name!"$ENDCOLOR
  exit 1
fi

if [ "$DELETE" == 0 ]; then
  # Create but vhost already exist
  if [ -f $APACHE_SITE_DIR/$VHOST.conf ]; then
    echo -e $RED"Vhost already exist!"$ENDCOLOR
    exit 1
  fi
fi

if [ "$DELETE" == 1 ]; then
  echo -e $YELLOW"Delete $VHOST database..."$ENDCOLOR
  $MYSQL -h $DB_HOST -u $DB_USER -p$DB_PASS -e "DROP DATABASE $VHOST;"
  echo -e $YELLOW"Delete $VHOST apache..."$ENDCOLOR
  $A2DIS $VHOST
  rm -f $APACHE_SITE_DIR/$VHOST.conf
  rm -rf $DOCUMENT_ROOT$VHOST
  service httpd restart
  echo -e $GREEN"$VHOST deleted!"$ENDCOLOR
  exit 0
fi

echo -e $YELLOW"Create vhost $VHOST..."$ENDCOLOR
cat > /tmp/$VHOST.conf << EOF
<VirtualHost *:$PORT>
  ServerAdmin $SERVER_ADMIN
  DocumentRoot $DOCUMENT_ROOT$VHOST
  ServerName $VHOST.$SUFFIX
  <Directory "$DOCUMENT_ROOT$VHOST">
    Options FollowSymLinks Indexes
    AllowOverride All
    Order deny,allow
    Allow from all
  </Directory>
  LogLevel warn
  #ErrorLog /var/log/httpd/${VHOST}_error.log
  #CustomLog /var/log/httpd/${VHOST}_access.log combined
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF

/usr/bin/mv /tmp/$VHOST.conf $APACHE_SITE_DIR/$VHOST.conf
$A2EN $VHOST
/usr/bin/mkdir $DOCUMENT_ROOT$VHOST
chown $APACHE_USER $DOCUMENT_ROOT$VHOST
service httpd restart

# Create database
$MYSQL -h $DB_HOST -u $DB_USER -p$DB_PASS -e "CREATE DATABASE $VHOST;"

echo -e $GREEN"Vhost $VHOST.dvm created!"$ENDCOLOR
exit
