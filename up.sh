#!/bin/bash

PROGNAME=${0##*/} 
PROGVERSION=0.1

YELLOW="\033[1;33m"
RED="\033[0;31m"
GREEN="\033[0;32m"
ENDCOLOR="\033[0m"

print_help() {
cat <<-HELP
Full Centos 7 LAMP stack update (Yum, Composer, Drush, Drupal console, symfony)
 Options:
  -h,  --help         Display this help and exit
  -V,  --version      Output version information and exit

Usage: (sudo) bash ${0##*/}

Example: (sudo) bash ${0##*/}

HELP
exit 0
}

# Check command flags invoked and store possible arguments.
for i in "$@"
do
case $i in
    -h|--help) print_help >&2; safe_exit ;;
    -V|--version) out "$PROGNAME v$PROGVERSION"; safe_exit ;;
esac
done

# Yum update
echo -e $YELLOW"Yum udpate..."$ENDCOLOR
#/usr/bin/yum -y update

# Composer / Drush
if hash composer 2>/dev/null; then
    echo -e $YELLOW"Composer/Drush udpate..."$ENDCOLOR
    /bin/rm -rf /root/.drush/cache/
    /usr/local/bin/composer clear-cache
    /usr/local/bin/composer self-update
    /usr/local/bin/composer global update
else
    echo -e $RED"Missing Composer bin"$ENDCOLOR
fi

# Drupal console
if hash drupal 2>/dev/null; then
    echo -e $YELLOW"Drupal console udpate..."$ENDCOLOR
    /usr/local/bin/drupal self-update
else
    echo -e $RED"Missing Drupal console bin"$ENDCOLOR
fi

# Symfony console
if hash symfony 2>/dev/null; then
    echo -e $YELLOW"Drupal console udpate..."$ENDCOLOR
    /usr/local/bin/symfony self-update
else
    echo -e $RED"Missing Drupal console bin"$ENDCOLOR
fi

echo -e $GREEN"Update Finished!"$ENDCOLOR
exit;