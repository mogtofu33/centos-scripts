#!/bin/bash
 
search_dir=$(pwd)
writable_dirs=$(find $search_dir -type d -perm 0777)
 
for dir in $writable_dirs
do
    #echo $dir
    find $dir -type f -name '*.php'
done